<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Componen */

$this->title = 'Create Componen';
$this->params['breadcrumbs'][] = ['label' => 'Componens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="componen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Validan */

$this->title = 'Create Validan';
$this->params['breadcrumbs'][] = ['label' => 'Validans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="validan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

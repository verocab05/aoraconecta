<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Validan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="validan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contrato')->textInput() ?>

    <?= $form->field($model, 'documento')->textInput() ?>

    <?= $form->field($model, 'valido')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

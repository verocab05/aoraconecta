<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contratan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contratan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cliente')->textInput() ?>

    <?= $form->field($model, 'componen')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'permanencia')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

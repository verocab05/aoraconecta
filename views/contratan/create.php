<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contratan */

$this->title = 'Create Contratan';
$this->params['breadcrumbs'][] = ['label' => 'Contratans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contratan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

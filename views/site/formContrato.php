<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Url;


?>

<head>
	<meta charset="UTF-8">
	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>-->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
       <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.m   in.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.theme.min.css"></script>
	<!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>-->
	<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>-->
	<link rel="stylesheet" type="text/css" href="<?= Url::to('@web/css/formulario.css')?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Datos | Contratar</title>
</head>
<body>
	<main>

		<div class="container">
			<div class="row">
				<div class="col-sm-12 cabecera">
					<img src="<?= Url::to('@web/imagenes/logo.png') ?>"/>
				</div>
			</div>

			<div class="bloque datosPer">Datos Personales
				<div class="col-12 datos_personales">
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
                                                </div>
						<input type="text" class="form-control" placeholder="Nombre">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Apellidos">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-edit"></i></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="NIF o NIE">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Teléfono de Contacto">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text">@</span>
   						</div>
						<input type="text" class="form-control" placeholder="Email">
					</div>
				</div>
                        </div>

			
                            <div class="bloque empresa">Empresas
				<div class="col-12 empresas">
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
    					</div>
						<input type="text" class="form-control" placeholder="Nombre de la Empresa">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-edit"></i></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="CIF">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Dirección de Facturación">
					</div>
				</div>
                            </div>

			
                             <div class="bloque dir">Dirección
				<div class="col-12 direccion">
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
    					</div>
						<input type="text" class="form-control" placeholder="Calle">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Número">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Piso">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Puerta">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Población">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Localidad">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-map-marker"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Código Postal">
					</div>
				</div>
                             </div>

			
                             <div class="bloque banca">Datos Bancarios
				<div class="col-12 banco">
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-book-open"></i></span>
    					</div>
						<input type="text" class="form-control" placeholder="Banco">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-book-open"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Número de Cuenta">
					</div>
					
				</div>
		
                             </div>     
			
                             <div class="bloque porta">Portabilidad Fijo y/o Móvil
				<div class="col-12 portabilidades">
					
					<h6>Portabilidad Fijo</h6>
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
    					</div>
						<input type="text" class="form-control" placeholder="Número de Teléfono">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-phone"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Compañía Actual">
					</div>

					<h6>Portabilidad Móvil</h6>
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
    					</div>
						<input type="text" class="form-control" placeholder="Número de Teléfono">
					</div>
					
					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Compañía Actual">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="Postpago o Prepago">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-addon">
							<span class="input-group-text"><i class="fa fa-mobile"></i></span>
   						</div>
						<input type="text" class="form-control" placeholder="ICC Tarjeta SIM">
					</div>
					
				</div>
			
                        </div>
			<div class="row enviar">
				<button type="button" class="btn btn-info btn-block">Enviar</button>
			</div>


		</div>
	</main> 
</body>
</html>


<script>
     $(document).ready(function() {
          $(".col-12").toggle();
           
            
          
          
         $(".bloque").click(function(){
             let clase = $(".bloque").children("div").attr('class').split(" ")[1];
//             console.log(clase);
//             let nomclas = clase.attr('class').split(" ")[1];
            
               $("."+clase).toggle( 'slow', function(){
                   console.log(clase);
                
               });
            });
                
     });
    
   // $(document).ready(function() {
        
        //$('.col-12').css('display','none');

//        $('.toggleable').click(function() {
//             $('.toggleable').toggle();
//        });
        
//    window.addEventListener('load',(e)=>{
//        
//       var elemento = document.querySelectorAll('.toggleable');
//       
//       
//       var elemento_visible = document.querySelectorAll('.col-12');
//       for(let el=0;el < elemento_visible.length;el++){
//           elemento_visible[el].style.display = "none";
//       }
//       
//       for (let c=0;c<elemento.length;c++){
//             elemento[c].addEventListener('click',(ev)=>{
//              
//               divdatos = document.querySelector('.'+clase);
////               console.log(divdatos);
//                 if (divdatos.style.display === "none") {
//                    divdatos.style.display = "block";
//                  } else {
//                    divdatos.style.display = "none";
//                }
//               
//           });
//           
//       }
       
       
       
//       console.log(elemento);
//       
//            elemento.addEventListener('click',(ev)=>{
//                divdatos = document.querySelector('.datos_personales');
//               if (divdatos.style.display === "none") {
//                    divdatos.style.display = "block";
//                  } else {
//                    divdatos.style.display = "none";
//                }
//               // datospersonales.classList.toggle();
//           
//       });
        
         
//    });
    
</script>
<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Url;
use yii\widgets\ListView;

?>

        <link rel="stylesheet" href="<?= Url::to('@web/css/servicios_bootstrap.css') ?>">
	<link href="http://allfont.es/allfont.css?fonts=montserrat" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Servicios</title>
        
        
  <main>      


  <div class="cabecera">
    <img class="logo" src="<?= Url::to('@web/imagenes/logo.png') ?>"/>
  </div>

    <div class="body-content">

	
 
 <?= ListView::widget([
        'dataProvider' => $resultados,
         'itemView' => '_bloques',
         'layout'=>"{pager}{items}",
      
    ]);
        
    ?>
     
</div>
<!--    <div class="servicios-index">

      <div class="row">

        <div class="col-md-6 col-sm-12 pie">
                <span class="textoazul">Información/Soporte</span><br/>
                <span><i class="fa fa-phone"></i> &nbsp; 942231563</span><br/>
                <span><i class="fa fa-phone"></i> &nbsp; 942049655</span><br/>
                <i class="fa fa-envelope"></i> &nbsp; <span class="textoazul">info@aoraconecta.com</span><br>
                <img src="<?= Url::to('@web/imagenes/logo_fondonegro.png')?>"/><br/><br/>
        </div>
        <div class="col-md-6 col-sm-12 pie2">
                <a href="https://www.facebook.com/aoraconecta"><img src="<?= Url::to('@web/imagenes/facebook.png')?>" alt="facebook"/></a>
                <a href="https://twitter.com/aoraconecta"><img src="<?= Url::to('@web/imagenes/twitter.png')?>" alt="twitter"/></a>
                <a href="https://www.instagram.com/aoraconecta"><img src="<?= Url::to('@web/imagenes/instagram.png')?>" alt="instagram"/></a>
        </div>

    </div>
	


    </div>-->

</main>

<!--<script>
    window.addEventListener('load',(e)=>{
      
        let botones =  document.querySelectorAll('.bloque_servicio');
        for(let c=0;c < botones.length;c++){
          botones[c].addEventListener("click", (ev)=>{  
          let codigo= botones[c].querySelector("#id_servicio").innerText;
        
          });
      }
   
   
//    window.location.replace("Url::toRoute('site/planes')");
   }); 


    
</script>-->
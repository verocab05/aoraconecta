<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $nif
 * @property string|null $direccion
 * @property int|null $cp
 * @property string|null $poblacion
 * @property string|null $provincia
 * @property int|null $movil
 * @property string|null $email
 * @property string|null $tipo
 * @property string|null $observaciones
 *
 * @property Contratan[] $contratans
 * @property Cuentas[] $cuentas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp', 'movil'], 'integer'],
            [['observaciones'], 'string'],
            [['nombre', 'apellidos', 'poblacion', 'provincia'], 'string', 'max' => 100],
            [['nif'], 'string', 'max' => 10],
            [['direccion', 'email'], 'string', 'max' => 200],
            [['tipo'], 'string', 'max' => 50],
            [['nif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'nif' => 'Nif',
            'direccion' => 'Direccion',
            'cp' => 'Cp',
            'poblacion' => 'Poblacion',
            'provincia' => 'Provincia',
            'movil' => 'Movil',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * Gets query for [[Contratans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratans()
    {
        return $this->hasMany(Contratan::className(), ['cliente' => 'id']);
    }

    /**
     * Gets query for [[Cuentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentas()
    {
        return $this->hasMany(Cuentas::className(), ['cliente' => 'id']);
    }
}

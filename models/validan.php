<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "validan".
 *
 * @property int $id
 * @property int $contrato
 * @property resource|null $documento
 * @property int|null $valido
 *
 * @property Contratan $contrato0
 */
class validan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'validan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contrato'], 'required'],
            [['contrato', 'valido'], 'integer'],
            [['documento'], 'string'],
            [['contrato'], 'exist', 'skipOnError' => true, 'targetClass' => Contratan::className(), 'targetAttribute' => ['contrato' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contrato' => 'Contrato',
            'documento' => 'Documento',
            'valido' => 'Valido',
        ];
    }

    /**
     * Gets query for [[Contrato0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContrato0()
    {
        return $this->hasOne(Contratan::className(), ['id' => 'contrato']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuentas".
 *
 * @property int $id
 * @property int $cliente
 * @property string $codpais
 * @property int $dciban
 * @property int $codigo
 * @property int $sucursal
 * @property int $dc
 * @property int $cuenta
 *
 * @property Clientes $cliente0
 */
class cuentas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuentas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente', 'codpais', 'dciban', 'codigo', 'sucursal', 'dc', 'cuenta'], 'required'],
            [['cliente', 'dciban', 'codigo', 'sucursal', 'dc', 'cuenta'], 'integer'],
            [['codpais'], 'string', 'max' => 3],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente' => 'Cliente',
            'codpais' => 'Codpais',
            'dciban' => 'Dciban',
            'codigo' => 'Codigo',
            'sucursal' => 'Sucursal',
            'dc' => 'Dc',
            'cuenta' => 'Cuenta',
        ];
    }

    /**
     * Gets query for [[Cliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente']);
    }
}

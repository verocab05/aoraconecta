<?php

namespace app\models;
use app\models\servicios;
use app\models\planes;

use Yii;

/**
 * This is the model class for table "componen".
 *
 * @property int $id
 * @property int $servicio
 * @property int $plan
 * @property string $descripcion
 * @property float $tarifa
 * @property int|null $activo
 *
 * @property Planes $plan0
 * @property Servicios $servicio0
 */
class componen extends \yii\db\ActiveRecord
{
    public $id_plan;
    public $tipo;
    public $titulo;
    public $velocidad;
    public $llamadas;
    public $datos;
    public $descripcion;

    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'componen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servicio', 'plan', 'descripcion', 'tarifa'], 'required'],
            [['servicio', 'plan', 'activo'], 'integer'],
            [['tarifa'], 'number'],
            [['descripcion'], 'string', 'max' => 200],
            [['servicio', 'plan'], 'unique', 'targetAttribute' => ['servicio', 'plan']],
            [['plan'], 'exist', 'skipOnError' => true, 'targetClass' => Planes::className(), 'targetAttribute' => ['plan' => 'id']],
            [['servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicios::className(), 'targetAttribute' => ['servicio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio' => 'Servicio',
            'plan' => 'Plan',
            'descripcion' => 'Descripcion',
            'tarifa' => 'Tarifa',
            'activo' => 'Activo',
        ];
    }

    /**
     * Gets query for [[Plan0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlan0()
    {
        return $this->hasOne(Planes::className(), ['id' => 'plan']);
    }

    /**
     * Gets query for [[Servicio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio0()
    {
        return $this->hasOne(Servicios::className(), ['id' => 'servicio']);
    }
    
     public function getServicios()
    {
        return $this->hasMany(Servicios::className(), ['id' => 'servicio']);
    }
       public function getPlanes()
    {
        return $this->hasMany(Planes::className(), ['id' => 'plan']);
    }
    
    
    
}

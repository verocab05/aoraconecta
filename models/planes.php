<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "planes".
 *
 * @property int $id
 * @property string $tipo
 * @property int $velocidad
 * @property string $descripcion
 * @property int|null $activo
 *
 * @property Componen[] $componens
 * @property Servicios[] $servicios
 */
class planes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'planes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'titulo','velocidad','llamadas','datos', 'descripcion'], 'required'],
            [['velocidad','llamadas','datos', 'activo'], 'integer'],
            [['tipo'], 'string', 'max' => 50],
            [['descripcion','titulo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'titulo' => 'Titulo',
            'velocidad' => 'Velocidad',
            'llamadas' => 'Llamadas',
            'datos' => 'Datos',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
        ];
    }

    /**
     * Gets query for [[Componens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponens()
    {
        return $this->hasMany(Componen::className(), ['plan' => 'id']);
    }

    /**
     * Gets query for [[Servicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicios::className(), ['id' => 'servicio'])->viaTable('componen', ['plan' => 'id']);
    }
}

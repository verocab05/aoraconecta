-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-02-2020 a las 17:51:26
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aoraconecta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `nif` varchar(10) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `provincia` varchar(100) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componen`
--

CREATE TABLE `componen` (
  `id` int(11) NOT NULL,
  `servicio` int(11) NOT NULL,
  `plan` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `tarifa` double(4,2) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `componen`
--

INSERT INTO `componen` (`id`, `servicio`, `plan`, `descripcion`, `tarifa`, `activo`) VALUES
(1, 1, 1, 'Internet Fijo 3 MB de bajada y 1 MB de subida', 15.00, 1),
(2, 1, 2, 'Cuota 18 €/mes IVA incluido a partir de los 12 meses', 18.00, 1),
(3, 1, 3, 'Cuota 28 €/mes IVA incluido a partir de los 12 meses', 30.00, 1),
(4, 1, 4, 'Cuota 33 €/mes IVA incluido a partir de los 12 meses', 35.00, 1),
(5, 1, 5, 'Cuota 38 €/mes IVA incluido a partir de los 12 meses', 42.00, 1),
(6, 2, 6, 'Internet Fijo 3 MB de bajada y 1 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 24.00, 1),
(7, 2, 7, 'Internet Fijo 6 MB de bajada y 2 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales.', 30.00, 1),
(8, 2, 8, 'Internet Fijo 10 MB de bajada y 3 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 40.00, 1),
(9, 2, 9, 'Internet Fijo 20 MB de bajada y 4 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 45.00, 1),
(10, 2, 10, 'Internet Fijo 30 MB de bajada y 5 MB de subida', 0.00, 0),
(11, 3, 11, 'Voz 100.  100 min. en llamadas.', 3.90, 1),
(12, 3, 12, 'Voz 200  200 min. en llamadas.', 4.90, 1),
(13, 3, 13, '300 min. en llamadas. Datos 5 GB.', 7.90, 1),
(14, 3, 14, 'Minutos ilimitados en llamadas. Datos 7 GB.100 sms', 9.90, 1),
(15, 3, 15, 'Minutos ilimitados en llamadas. Datos 10 GB. 1000 sms.', 10.90, 1),
(16, 3, 16, 'Minutos ilimitados en llamadas. Datos 12 GB. 100 sms.', 12.90, 1),
(17, 3, 17, 'Minutos ilimitados en llamadas. Datos 20 GB. 1000 sms.', 14.90, 1),
(18, 3, 18, 'Minutos ilimitados en llamadas. Datos 30 GB. 1000 sms.', 19.90, 1),
(19, 4, 19, 'ibra 100 MB.', 20.90, 1),
(20, 4, 20, 'Fibra 100Mb + línea móvil con llamadas ilimitadas y 10 GB. de datos', 29.90, 1),
(21, 4, 21, 'Fibra 100Mb + línea móvil con llamadas ilimitadas y 20 GB. de datos', 34.90, 1),
(22, 4, 22, 'Fibra 100Mb + teléfono fijo + línea móvil con llamadas ilimitadas y 10 GB. de datos', 39.90, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratan`
--

CREATE TABLE `contratan` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `componen` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `permanencia` datetime NOT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `codpais` char(3) NOT NULL,
  `dciban` int(3) NOT NULL,
  `codigo` int(5) NOT NULL,
  `sucursal` int(5) NOT NULL,
  `dc` int(2) NOT NULL,
  `cuenta` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `velocidad` int(5) NOT NULL,
  `llamadas` int(10) NOT NULL,
  `datos` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `tipo`, `velocidad`, `llamadas`, `datos`, `descripcion`, `activo`) VALUES
(1, 'INTERNET', 3, 0, 0, 'INTERNET A 3MB', 1),
(2, 'INTERNET', 6, 0, 0, 'INTERNET A 6MB', 1),
(3, 'INTERNET', 10, 0, 0, 'INTERNET A 10MB', 1),
(4, 'INTERNET', 20, 0, 0, 'INTERNET A 20MB', 1),
(5, 'INTERNET', 30, 0, 0, 'INTERNET A 30MB', 1),
(6, 'INTERNET + FIJO', 3, 1000, 0, 'Internet Fijo 3 MB de bajada y 1 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 1),
(7, 'INTERNET + FIJO', 6, 1000, 0, 'Internet Fijo 6 MB de bajada y 2 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 1),
(8, 'INTERNET + FIJO', 10, 1000, 0, 'Internet Fijo 10 MB de bajada y 3 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 1),
(9, 'INTERNET + FIJO', 20, 1000, 0, 'Internet Fijo 20 MB de bajada y 4 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 1),
(10, 'INTERNET + FIJO', 30, 1000, 0, 'Internet Fijo 30 MB de bajada y 5 MB de subida. 1000 minutos en llamadas a fijos y móviles nacionales', 1),
(11, 'VOZ100', 0, 100, 0, '100 min. en llamadas', 1),
(12, 'VOZ200', 0, 200, 0, '200 min. en llamadas.', 1),
(13, 'MOVIL', 0, 300, 5, '300 minutos + 5 GB', 1),
(14, 'MOVIL', 0, 9999999, 7, 'Minutos ilimitados + 7 GB', 1),
(15, 'MOVIL', 0, 9999999, 10, 'Minutos ilimitados + 10 GB', 1),
(16, 'MOVIL', 0, 9999999, 12, 'Minutos ilimitados + 12 GB', 1),
(17, 'MOVIL', 0, 9999999, 20, 'Minutos ilimitados + 20 GB', 1),
(18, 'MOVIL', 0, 9999999, 30, 'Minutos ilimitados + 30 GB', 1),
(19, 'FIBRA', 100, 0, 0, 'Fibra 100 MB', 1),
(20, 'FIBRA', 100, 9999999, 10, 'Fibra 100 MB + móvil llamadas ilimitadas con 10 GB', 1),
(21, 'FIBRA', 100, 9999999, 20, 'Fibra 100 MB + móvil llamadas ilimitadas con 20 GB', 1),
(22, 'FIBRA', 100, 9999999, 10, 'Fibra 100 MB + teléfono fijo + móvil llamadas ilimitadas con 10 GB', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL,
  `referencia` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `rutaimg` varchar(200) NOT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `referencia`, `nombre`, `descripcion`, `rutaimg`, `activo`) VALUES
(1, 'INTERNET', 'INTERNET', 'SOLO INTERNET', '', 1),
(2, 'INTERFIJO', 'INTERNET + FIJO', 'INTERNET MAS TELEFONO FIJO', '', 1),
(3, 'MOV', 'MOVIL', 'SOLO TELEFONIA MOVIL', '', 1),
(4, 'FIB', 'FIBRA', 'TECNOLOGIA DE FIBRA', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `validan`
--

CREATE TABLE `validan` (
  `id` int(11) NOT NULL,
  `contrato` int(11) NOT NULL,
  `documento` longblob DEFAULT NULL,
  `valido` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nif` (`nif`);

--
-- Indices de la tabla `componen`
--
ALTER TABLE `componen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `servicio` (`servicio`,`plan`),
  ADD KEY `FK_Componenplanes` (`plan`);

--
-- Indices de la tabla `contratan`
--
ALTER TABLE `contratan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ContratanClientes` (`cliente`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ClientesCuentas` (`cliente`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `validan`
--
ALTER TABLE `validan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ValidanContratos` (`contrato`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `componen`
--
ALTER TABLE `componen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `contratan`
--
ALTER TABLE `contratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `validan`
--
ALTER TABLE `validan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `componen`
--
ALTER TABLE `componen`
  ADD CONSTRAINT `FK_Componenplanes` FOREIGN KEY (`plan`) REFERENCES `planes` (`id`),
  ADD CONSTRAINT `FK_Componenservicios` FOREIGN KEY (`servicio`) REFERENCES `servicios` (`id`);

--
-- Filtros para la tabla `contratan`
--
ALTER TABLE `contratan`
  ADD CONSTRAINT `FK_ContratanClientes` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `FK_ClientesCuentas` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `validan`
--
ALTER TABLE `validan`
  ADD CONSTRAINT `FK_ValidanContratos` FOREIGN KEY (`contrato`) REFERENCES `contratan` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
